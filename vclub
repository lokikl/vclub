#!/usr/bin/env python
# encoding: utf-8

import sys
import socket
import getopt
import threading
import netifaces
import pdb
import time
import subprocess

from netifaces import AF_INET, AF_INET6, AF_LINK, AF_PACKET, AF_BRIDGE

ALLOW_CONTROL_PASSCODE = "allow-control"
DISALLOW_CONTROL_PASSCODE = "disallow-control"

bind_port   = 63481
hosts       = []

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)


def get_broadcast_ip():
    interfaces = netifaces.interfaces()
    for i in ['br-eth0', 'eth0', 'p2p1', 'p1p1']:
        if i in interfaces:
            nics = netifaces.ifaddresses(i)[AF_INET]
            nics = [n for n in nics if 'broadcast' in n]
            return nics[0]['broadcast']
    raise NameError("No network interfaces found, expected eth0 / p2p1 / p1p1")


def setup_vnc_server():
    global read_only
    gsettings = [
        ["require-encryption", "false"],
        ["notify-on-connect", "true"],
        ["disable-background", "false"],
        ["prompt-enabled", "false"],
        ["enabled", "true"],
    ]
    for s in gsettings:
        c = ' '.join(i for i in s)
        c = "gsettings set org.gnome.Vino " + c
        print "[e] " + c
        subprocess.Popen(c, shell=True)

def invite_hosts(ihosts):
    for host in ihosts:
        print "[*] send request to host " + host
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((host, bind_port))
        sock.send("invite " + socket.gethostname())
        sock.close()

def discover_hosts():
    del hosts[:]
    bip = get_broadcast_ip()
    broadcast_dest = (bip, bind_port)
    print "[*] Sending discovery request: UDP %s:%d" % broadcast_dest
    udp_socket.sendto("host discovery", broadcast_dest)
    subprocess.Popen("zenity --progress --pulsate --no-cancel --auto-close --title='Discovering' --text=''",
        shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    time.sleep(2) # wait a moment for clients to feedback
    subprocess.call(["pkill", "zenity"]) # kill the dialog
    hosts_str = ' '.join(x for x in list(set(hosts)))
    if len(hosts) == 0:
        print "[!] No hosts found"
    else:
        list_proc = subprocess.Popen(
            "zenity --list --multiple --title='Invite Hosts' " +
            "--text \"Select hosts to invite\" " +
            "--width=380 --height=430 " +
            "--print-column=2 " +
            "--column \"Host\" --column \"IP\" " +
            hosts_str,
            shell=True,
            stdout=subprocess.PIPE)
        list_proc.wait()
        selected_hosts = list_proc.stdout.read().rstrip("\r\n").split('|')
        if selected_hosts[0] != '':
            setup_vnc_server()
            invite_hosts(list(set(selected_hosts)))
        else:
            print "[.] no hosts selected"

# this is our client handling thread
def handle_client(client_socket, addr):
    message = client_socket.recv(1024)
    client_socket.close()
    print "[*] Received: %s" % message
    if message == ALLOW_CONTROL_PASSCODE:
        subprocess.Popen("gsettings set org.gnome.Vino view-only false" , shell=True)
        discover_hosts()
    elif message == DISALLOW_CONTROL_PASSCODE:
        subprocess.Popen("gsettings set org.gnome.Vino view-only true" , shell=True)
        discover_hosts()
    elif message.split(' ')[0] == "invite":
        host = message.split(' ')[1]
        proc = subprocess.Popen('zenity --question --text="Screen sharing invitation from ' + host + ', accept?"', shell=True)
        subprocess.Popen('type wmctrl > /dev/null && wmctrl -a "Question"', shell=True).communicate()[0]
        proc.communicate()[0]
        if proc.returncode == 0:
            print "[*] starting vncviewer to " + addr[0]
            subprocess.call(["vncviewer", addr[0]])
    else:
        if socket.gethostname() != message:
            hosts.append(message + " " + addr[0])

def start_tcp_server():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    bind_ip = "0.0.0.0"
    server.bind((bind_ip, bind_port))
    server.listen(5)
    print "[*] Listening from: TCP %s:%d" % (bind_ip, bind_port)

    try:
        while True:
            client, addr = server.accept()
            print "[*] TCP connection from: %s:%d" % (addr[0], addr[1])
            # spin up our client thread to handle incoming data
            client_handler = threading.Thread(target=handle_client,args=(client,addr,))
            client_handler.start()
    except KeyboardInterrupt:
        pass

    server.close()

def handle_udp_client(bs, message, address):
    print "[u] Got data [%s] from %s" % (message, address)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(address)
    sock.send(socket.gethostname())
    sock.close()

def start_broadcast_server():
    bip = get_broadcast_ip()
    broadcast_dest = (bip, bind_port)
    print "[u] Listening from: UDP %s:%d" % broadcast_dest
    udp_socket.bind(broadcast_dest)
    while 1:
        message, address = udp_socket.recvfrom(4096)
        client_handler = threading.Thread(target=handle_udp_client,args=(udp_socket, message, address,))
        client_handler.start()


def usage():
    print "VClub Server & Client"
    print
    print "Usage: vclub"
    print "-h --help                - show this help message"
    print "-s --server              - start a vclub server"
    print "-c --client              - initialize a sharing session"
    print "-a --allow-control       - allow others to control"
    print
    print
    print "Examples: "
    print "vclub -s"
    print "vclub -c"
    print "vclub -ca"
    sys.exit(0)

def main():
    if not len(sys.argv[1:]): usage()

    try:
        opts, args = getopt.getopt(sys.argv[1:],"hsca",["help","server","client","allow-control"])
    except getopt.GetoptError as err:
        print str(err)
        usage()

    server_mode = False
    allow_control = False
    for o,a in opts:
        if o in ("-h","--help"):
            usage()
        elif o in ("-s","--server"):
            server_mode = True
        elif o in ("-c", "--client"):
            server_mode = False
        elif o in ("-a", "--allow-control"):
            allow_control = True
        else:
            assert False,"Unhandled Option"

    if server_mode:
        udp = threading.Thread(target=start_broadcast_server)
        udp.setDaemon(True)
        udp.start()

        start_tcp_server()
    else: # client mode to trigger sharing
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(("127.0.0.1", bind_port))
        if allow_control:
            sock.send(ALLOW_CONTROL_PASSCODE)
        else:
            sock.send(DISALLOW_CONTROL_PASSCODE)
        sock.close()

main()
